window.onload = () => {
	let button = document.querySelector("#btn");

	button.addEventListener("click", calculateBMI);
};

function calculateBMI() {


	let Gjatesia = parseInt(document.querySelector("#Gjatesia").value);


	let pesha = parseInt(document.querySelector("#pesha").value);

	let result = document.querySelector("#result");


	if (Gjatesia === "" || isNaN(Gjatesia))
		result.innerHTML = "Jep gjatesi valide!";

	else if (pesha === "" || isNaN(pesha))
		result.innerHTML = "Jep peshe valide!";

	else {

		let bmi = (pesha / ((Gjatesia * Gjatesia)
							/ 10000)).toFixed(2);

		if (bmi < 18.6) result.innerHTML =
			`Duhet te shtoni peshe : ${bmi}`;

		else if (bmi >= 18.6 && bmi < 24.9)
			result.innerHTML =
				`pseha juaj eshte normale : ${bmi}`;

		else result.innerHTML =
			`Mbi peshe : ${bmi}`;
	}
}
