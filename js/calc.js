
function add() {
    let nr1 = parseInt(document.querySelector("#nr1").value);


    let nr2 = parseInt(document.querySelector("#nr2").value);

    let rezultati = document.querySelector("#rezultati");


    if (nr1 === "" || isNaN(nr1))
        rezultati.innerHTML = "Jep numer valid!";

    else if (nr2 === "" || isNaN(nr2))
        rezultati.innerHTML = "Jep numer valid!";
    else {
        rezultati.innerHTML = nr1 + nr2;
    }
}

function minus() {
    let nr1 = parseInt(document.querySelector("#nr1").value);


    let nr2 = parseInt(document.querySelector("#nr2").value);

    let rezultati = document.querySelector("#rezultati");


    if (nr1 === "" || isNaN(nr1))
        rezultati.innerHTML = "Jep numer valid!";

    else if (nr2 === "" || isNaN(nr2))
        rezultati.innerHTML = "Jep numer valid!";
    else {
        rezultati.innerHTML = nr1 - nr2;
    }
}

function shumezim() {
    let nr1 = parseInt(document.querySelector("#nr1").value);


    let nr2 = parseInt(document.querySelector("#nr2").value);

    let rezultati = document.querySelector("#rezultati");


    if (nr1 === "" || isNaN(nr1))
        rezultati.innerHTML = "Jep numer valid!";

    else if (nr2 === "" || isNaN(nr2))
        rezultati.innerHTML = "Jep numer valid!";
    else {
        rezultati.innerHTML = nr1 * nr2;
    }
}

function pjestim() {
    let nr1 = parseInt(document.querySelector("#nr1").value);


    let nr2 = parseInt(document.querySelector("#nr2").value);

    let rezultati = document.querySelector("#rezultati");


    if (nr1 === "" || isNaN(nr1))
        rezultati.innerHTML = "Jep numer valid!";

    else if (nr2 === "" || isNaN(nr2))
        rezultati.innerHTML = "Jep numer valid!";
    else {
        rezultati.innerHTML = nr1 / nr2;
    }
}
